FROM debian:stretch-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    bash \
    wget \
    git \
    build-essential \
    devscripts \
    debhelper \
    docbook-to-man \
    libx11-dev \
    x11proto-core-dev \
    libz-dev \
    quilt \
    fakeroot

ADD . /infinality

RUN find /infinality/freetype-infinality/debian \
      -maxdepth 1 \
      -type f \
      -name '*freetype-*' \
      -exec chmod -x {} \; && \
    cd /infinality && \
    mkdir dist && \
    cd freetype-infinality && \
    ./build.sh && \
    mv *.deb ../dist && \
    cd ../fontconfig-infinality && \
    ./build.sh && \
    mv *.deb ../dist
