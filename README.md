# Build Infinality Debian packages

This repo is a fork of [Ha-Duong Nguyen](https://github.com/cmpitg)'s [repo](https://github.com/cmpitg/infinality-debian-package).
I turned the instructions into a Dockerfile which generates the .debs.

## Introduction

[Infinality](http://www.infinality.net/blog/) is a collection of patches
aiming to provide better font rendering and customization, and thus fixing
this problem for GNU/Linux distros.  Go for its homepage for further
information.

## Build 

Run `./bin/build`

.deb files will be in `dist`

## Install

`sudo dpkg -i \
    freetype-infinality/*.deb \
    fontconfig-infinality/*.deb`

Reboot (is this really necessary)?

Then tweak your settings:

* `bash /etc/fonts/infinality/infctl.sh setstyle`
* Edit `/etc/profile.d/infinality-settings.sh`.
